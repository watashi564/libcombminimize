package libcombminimize

import (
	"bytes"
	"io/ioutil"
	"strings"
	"testing"
)

func TestDriverV1Func(t *testing.T) {
	var testCases = map[string]string{
		`{"CombMinimizeV1":{
			"01": [13,14,15,16],
			"02": [17,18],
			"01": ["03",30,"02"],
			"02": ["04",10,"03"],
			"03": []	
		}}`: `{"CombSolutionV1":{"Keep":["01","02",14,16,13,15],"Remove":[17,18]}}`,
		`{"CombMinimizeV1":{
			"01": [13,14,15,16],
			"02": [17,18],
			"01": ["03",10,"20"],
			"02": ["03",15,"20"],
			"03": ["04",7,"20"],
			"04": [9,19,20],
			"04": ["05",8,"20"],
			"05": ["20",17,"06"],
			"06": []
		}}`: `{"CombSolutionV1":{"Keep":["05","04",19,9],"Remove":["01","03","02",20,13,16,14,18,17,15]}}`,
	}
	for k, v := range testCases {
		var buf = bytes.NewBuffer(nil)
		DriverV1(strings.NewReader(k), buf)
		var res, err = ioutil.ReadAll(buf)
		if err != nil {
			t.Error(err)
			return
		}
		if len(res) != len(v) {
			println(v)
			panic(string(res))
		}
	}
}
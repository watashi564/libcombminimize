# LibCombMinimize #

This is a code for minimizing haircomb histories. This adds an extra privacy when paying someone.

### What is this repository for? ###

Preview of something.

### How do I get set up? ###

* Install python
* Install Google OR Tools
* Try to run this (experimental)

## Request format

It's a json/jsonp based format. An empty skeleton looks like this:

`{"CombMinimizeV1":{}}`

Now, Inside can be many of several kinds of objects:

* combbase
* stack
* transaction
* target

### Request combbase

`{"CombMinimizeV1":{"01":[500]}}`

This is just a 500 smallest units combbase, claimed to address "01". Decimals are unsupported, but integers are, up to 2^64-1.
*Note*: Each combbase must have a different amount.
*Note*: Each address must be of hex encoding, can be lower or upper case, but it must have an even length of hex characters.

### Request multiple combbase

`{"CombMinimizeV1":{"01":[500, 600]}}`

Two combbases were claimed to address "01", 500 and 600 smallest units.

### Request stack

`{"CombMinimizeV1":{"01":["02" 33 "03"]}}`

There is a stack at address "01", that pays 33 smallest units to "03" (if triggered) and then change to "02".
*Note*: Objects MUST appear here in the order changeAddr, amt, toAddr.

### Request transaction

`{"CombMinimizeV1":{"01":["02"]}}`

Just a transaction from an address 01 to an address 02.

### Request target

`{"CombMinimizeV1":{"01":[]}}`

The address 01 is now a target wallet to be minimized, there can be multiple targets.


## Response format

`{"CombSolutionV1":{"Keep":["05","04",19,9],"Remove":["01","03","02",20,13,16,14,18,17,15]}}`

There are two sets in the response, the set of combbases and stacks to keep, and the set of combbases and stacks to remove.

*Note*: 0 amount stacks are internally turned into tx, that's why they would not appear in either list.



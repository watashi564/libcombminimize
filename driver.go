package libcombminimize

import (
	"bitbucket.org/watashi564/libcombminimize/callpython"
	"bitbucket.org/watashi564/libcombminimize/decenc"
	"bitbucket.org/watashi564/libcombminimize/flow"
	"io"
)

func DriverV1(r io.Reader, w io.Writer) {
	var p decenc.LibMinParser
	var seq = decenc.NewStringSequence()
	p.Sq = &seq
	err1 := decenc.MinimizeDeserializeV1(r, &p)
	if err1 != nil {
		panic(err1.Error())
	}
	if p.Req == nil {
		panic("nil request")
	}
	seq.Complete()
	err6 := seq.UnHex()
	if err6 != nil {
		panic(err6.Error())
	}
	var pyScript, err2 = flow.Process(
		p.Req.CombMinimize.Base,
		p.Req.CombMinimize.StackTo,
		p.Req.CombMinimize.StackAmount,
		p.Req.CombMinimize.StackChange,
		p.Req.CombMinimize.Target)
	if err2 != nil {
		panic(err2.Error())
	}
	var responses, err3 = callpython.CallPython(pyScript, callpython.PythonBinary)
	if err3 != nil {
		panic(err3.Error())
	}
	p.Resp = new(decenc.CombResponse)
	var err5 error
	p.Resp.CombSolution.KeepStacks,
		p.Resp.CombSolution.KeepBases,
		p.Resp.CombSolution.RemoveStacks,
		p.Resp.CombSolution.RemoveBases,
		err5 = flow.Postprocess(responses, p.Req.CombMinimize.Base, p.Req.CombMinimize.StackAmount)
	if err5 != nil {
		panic(err5.Error())
	}
	var resp = decenc.NewResponseEncoder(p.Req.CombJsonP.Data, p.Req.CombJsonP.IsFunc)
	for k := range p.Resp.CombSolution.KeepStacks {
		resp.AddStack([]byte(seq.Get(k)))
	}
	for k := range p.Resp.CombSolution.KeepBases {
		resp.AddBase(k)
	}
	var resp2 = resp.Complete()
	for k := range p.Resp.CombSolution.RemoveStacks {
		resp2.AddStack([]byte(seq.Get(k)))
	}
	for k := range p.Resp.CombSolution.RemoveBases {
		resp2.AddBase(k)
	}
	ret, err4 := resp2.Complete(true)
	if err4 != nil {
		panic(err4.Error())
	}
	_, err7 := w.Write(ret)
	if err7 != nil {
		panic(err7.Error())
	}
}
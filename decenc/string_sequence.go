package decenc

import (
	"encoding/hex"
)

type StringSequence struct {
	Seq []string
	Map map[string]uint64
}

// NewStringSequence constructs a new string sequence. The empty string is added with ID of 0.
func NewStringSequence() (sseq StringSequence) {
	sseq.Map = make(map[string]uint64)
	sseq.Add("")
	return sseq
}

// Add adds a string to a string sequence, returning it's ID number
func (sseq *StringSequence) Add(str string) uint64 {
	if sseq.Map == nil {
		sseq.Seq = append(sseq.Seq, str)
		return uint64(len(sseq.Seq)) - 1
	}
	var pos, ok = sseq.Map[str]
	if ok {
		return pos
	}
	pos = uint64(len(sseq.Seq))
	sseq.Seq = append(sseq.Seq, str)
	sseq.Map[str] = pos
	return pos
}

// Complete signals that no more strings will be added to the string sequence
func (sseq *StringSequence) Complete() {
	sseq.Map = nil
}

// Get gets the ID of a string from the sequence
func (sseq *StringSequence) Get(pos uint64) string {
	return sseq.Seq[pos]
}

// UnHex attempts to decode all identifiers from hex
// it skips identifiers that cannot be decoded
func (sseq *StringSequence) UnHex() (err1 error) {
	for i, v := range sseq.Seq {
		var buf = make([]byte, 2*len(v))
		n, err := hex.Decode(buf, []byte(v))
		if err != nil {
			err1 = err
		} else {
			sseq.Seq[i] = string(buf[0:n])
		}
	}
	if sseq.Map != nil {
		sseq.Map = make(map[string]uint64)
		for i, v := range sseq.Seq {
			sseq.Map[v] = uint64(i)
		}
	}
	return err1
}
package decenc

import (
	"encoding/binary"
	"strings"
)

type CombJsonP struct {
	Data   string
	IsFunc bool
}

type CombRequest struct {
	CombMinimize CombMinimize
	CombJsonP
}

type CombMinimize struct {
	Base        map[uint64]uint64
	StackTo     map[uint64]uint64
	StackChange map[uint64]uint64
	StackAmount map[uint64]uint64
	Target      map[uint64]struct{}
}

func NewCombRequest() *CombRequest {
	return &CombRequest{
		CombMinimize: CombMinimize{
			Base:        make(map[uint64]uint64),
			StackTo:     make(map[uint64]uint64),
			StackChange: make(map[uint64]uint64),
			StackAmount: make(map[uint64]uint64),
			Target:      make(map[uint64]struct{}),
		},
	}
}

func u64tobytes(k uint64) []byte {
	var buf [8]byte
	binary.LittleEndian.PutUint64(buf[:], k)
	return buf[:]
}

func (req *CombRequest) Encode(sseq *StringSequence) ([]byte, error) {
	var enc = NewRequestEncoder(req.CombJsonP.Data, req.CombJsonP.IsFunc)
	for k := range req.CombMinimize.Target {
		if sseq == nil {
			enc.Add(u64tobytes(k))
		} else {
			enc.Add([]byte(sseq.Get(k)))
		}
	}
	var enc2 = enc.Complete()
	for k, amt := range req.CombMinimize.StackAmount {
		var to = req.CombMinimize.StackTo[k]
		var chg = req.CombMinimize.StackChange[k]
		if sseq == nil {
			enc2.Add(u64tobytes(k), u64tobytes(to), u64tobytes(chg), amt)
		} else {
			enc2.Add([]byte(sseq.Get(k)), []byte(sseq.Get(to)), []byte(sseq.Get(chg)), amt)
		}
	}
	var enc3 = enc2.Complete()
	for amt, addr := range req.CombMinimize.Base {
		if sseq == nil {
			enc3.Add(u64tobytes(addr), amt)
		} else {
			enc3.Add([]byte(sseq.Get(addr)), amt)
		}
	}
	return enc3.Complete(true)
}

func (req *CombRequest) Equals(req2 *CombRequest) bool {
	if req.CombJsonP.IsFunc != req2.CombJsonP.IsFunc {
		//println("difference in IsFuncJsonP")
		return false
	}
	if strings.TrimSpace(req.CombJsonP.Data) != strings.TrimSpace(req2.CombJsonP.Data) {
		//println("difference in JsonP", req.Data, req2.Data)

		return false
	}
	if len(req.CombMinimize.Target) != len(req2.CombMinimize.Target) {
		//println("difference in Target", len(req.CombMinimize.Target), len(req2.CombMinimize.Target))

		return false
	}
	if len(req.CombMinimize.StackTo) != len(req2.CombMinimize.StackTo) {
		//println("difference in StackTo")

		return false
	}
	if len(req.CombMinimize.StackChange) != len(req2.CombMinimize.StackChange) {
		//println("difference in StackChange")

		return false
	}
	if len(req.CombMinimize.Base) != len(req2.CombMinimize.Base) {
		//println("difference in Base")

		return false
	}
	if len(req.CombMinimize.StackAmount) != len(req2.CombMinimize.StackAmount) {
		//println("difference in StackAmount")

		return false
	}
	var addrMap = make(map[uint64]uint64)
	for amt, addr := range req.CombMinimize.Base {
		otherAddr, ok := req2.CombMinimize.Base[amt]
		if !ok {
			//println("difference in Base mapping", amt, addr)

			return false
		}
		addrMap[addr] = otherAddr
	}
	for {
		var l = len(addrMap)
		for from, to := range req.CombMinimize.StackTo {
			otherFrom, ok1 := addrMap[from]
			otherTo, ok2 := req2.CombMinimize.StackTo[otherFrom]
			if ok1 && ok2 {
				addrMap[to] = otherTo
			}
		}
		for from, to := range req.CombMinimize.StackChange {
			otherFrom, ok1 := addrMap[from]
			otherTo, ok2 := req2.CombMinimize.StackChange[otherFrom]
			if ok1 && ok2 {
				addrMap[to] = otherTo
			}
		}
		if l == len(addrMap) {
			break
		}
	}
	for addr, amt := range req.CombMinimize.StackAmount {
		otherAddr, ok := addrMap[addr]
		if !ok {
			continue
		}
		otherAmt, ok := req2.CombMinimize.StackAmount[otherAddr]
		if !ok {
			//println("difference in Stack amount mapping amount")

			return false
		}
		if amt != otherAmt {
			//println("difference in Stack amount mapping amounts")

			return false

		}

	}

	return true
}

/*
{"CombSolutionV1":{"Remove":["768786786787", "bb"]}}
*/
type CombResponse struct {
	CombSolution CombSolution
	CombJsonP
}

type CombSolution struct {
	RemoveStacks map[uint64]struct{}
	RemoveBases  map[uint64]struct{}
	KeepStacks   map[uint64]struct{}
	KeepBases    map[uint64]struct{}
}

func NewCombResponse() *CombResponse {
	return &CombResponse{
		CombSolution: CombSolution{
			RemoveStacks: make(map[uint64]struct{}),
			RemoveBases:  make(map[uint64]struct{}),
			KeepStacks:   make(map[uint64]struct{}),
			KeepBases:    make(map[uint64]struct{}),
		},
	}
}

func (req *CombResponse) Encode(sseq *StringSequence) ([]byte, error) {
	var enc = NewResponseEncoder(req.CombJsonP.Data, req.CombJsonP.IsFunc)

	for k := range req.CombSolution.KeepStacks {
		if sseq == nil {
			enc.AddStack(u64tobytes(k))
		} else {
			enc.AddStack([]byte(sseq.Get(k)))
		}
	}

	for b := range req.CombSolution.KeepBases {
		enc.AddBase(b)
	}

	var enc2 = enc.Complete()

	for k := range req.CombSolution.RemoveStacks {
		if sseq == nil {
			enc2.AddStack(u64tobytes(k))
		} else {
			enc2.AddStack([]byte(sseq.Get(k)))
		}
	}

	for b := range req.CombSolution.RemoveBases {
		enc2.AddBase(b)
	}
	return enc2.Complete(true)
}

func (req *CombResponse) Equals(req2 *CombResponse) bool {
	if req.CombJsonP.IsFunc != req2.CombJsonP.IsFunc {
		//println("difference in IsFuncJsonP")
		return false
	}
	if strings.TrimSpace(req.CombJsonP.Data) != strings.TrimSpace(req2.CombJsonP.Data) {
		//println("difference in JsonP", req.Data, req2.Data)

		return false
	}
	if len(req.CombSolution.RemoveStacks) != len(req2.CombSolution.RemoveStacks) {
		//println("difference in RemoveStacks")

		return false
	}
	if len(req.CombSolution.KeepStacks) != len(req2.CombSolution.KeepStacks) {
		//println("difference in KeepStacks")

		return false
	}
	if len(req.CombSolution.RemoveBases) != len(req2.CombSolution.RemoveBases) {
		//println("difference in RemoveBases")

		return false
	}
	if len(req.CombSolution.KeepBases) != len(req2.CombSolution.KeepBases) {
		//println("difference in KeepBases")

		return false
	}
	for amt := range req.CombSolution.RemoveBases {
		_, ok := req2.CombSolution.RemoveBases[amt]
		if !ok {
			//println("difference in RemoveBases mapping")

			return false
		}
	}

	for amt := range req.CombSolution.KeepBases {
		_, ok := req2.CombSolution.KeepBases[amt]
		if !ok {
			//println("difference in RemoveBases mapping")

			return false
		}
	}

	return true
}
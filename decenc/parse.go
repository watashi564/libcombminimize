package decenc

import "errors"

type LibMinParser struct {
	Req  *CombRequest
	Resp *CombResponse
	Sq   *StringSequence
	Err  error
}

const VersionMinimize = `"CombMinimizeV1"`
const VersionSolution = `"CombSolutionV1"`

var ErrIntegerOverflow = errors.New("parser: integer overflow")

func TrimDq(s string) string {
	if len(s) < 2 {
		return s
	}
	if s[0] == '"' {
		s = s[1:]
	}
	if s[len(s)-1] == '"' {
		s = s[0 : len(s)-1]
	}
	return s
}
func Uint64decode(str string) (n uint64, err error) {
	for i, v := range str {
		if i >= 18 {
			return 0, ErrIntegerOverflow
		}
		switch v {
		case '.':
			return
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
			n *= 10
			n += uint64(str[i]) - '0'
		}
	}
	return n, nil
}

func (l *LibMinParser) ParseInit(s, s2 string, b bool) error {

	if s == VersionMinimize {
		l.Req = NewCombRequest()
		l.Req.CombJsonP.Data = s2
		l.Req.CombJsonP.IsFunc = b

		return nil
	}
	if s == VersionSolution {
		l.Resp = NewCombResponse()
		l.Resp.CombJsonP.Data = s2
		l.Resp.CombJsonP.IsFunc = b

		return nil
	}

	return nil
}

func (l *LibMinParser) ParseCombBase(s string, s1 string, s2 string) error {
	if s == VersionMinimize {
		var n2, err = Uint64decode(s2)
		if err != nil {
			return err
		}
		var n1 = l.Sq.Add(TrimDq(s1))
		l.Req.CombMinimize.Base[n2] = n1
	}
	if s == VersionSolution {
		var n2, err = Uint64decode(s2)
		if err != nil {
			return err
		}
		switch s1 {
		case "Keep":
			l.Resp.CombSolution.KeepBases[n2] = struct{}{}
		case "Remove":
			l.Resp.CombSolution.RemoveBases[n2] = struct{}{}
		}
	}
	return nil
}

func (l *LibMinParser) ParseCombStackTo(s string, s1 string, s2 string) error {
	if s == VersionMinimize {
		var n1 = l.Sq.Add(TrimDq(s1))
		var n2 = l.Sq.Add(TrimDq(s2))
		l.Req.CombMinimize.StackTo[n1] = n2
	}
	if s == VersionSolution {
		var n2 = l.Sq.Add(TrimDq(s2))
		switch s1 {
		case "Keep":
			l.Resp.CombSolution.KeepStacks[n2] = struct{}{}
		case "Remove":
			l.Resp.CombSolution.RemoveStacks[n2] = struct{}{}
		}
	}
	return nil
}

func (l *LibMinParser) ParseCombStackAmount(s string, s1 string, s2 string) error {
	if s == VersionMinimize {
		var n1 = l.Sq.Add(TrimDq(s1))
		var n2, err = Uint64decode(s2)
		if err != nil {
			return err
		}
		l.Req.CombMinimize.StackAmount[n1] = n2
		l.Req.CombMinimize.StackTo[n1] = 0
	}
	if s == VersionSolution {
		var n2, err = Uint64decode(s2)
		if err != nil {
			return err
		}
		switch s1 {
		case "Keep":
			l.Resp.CombSolution.KeepBases[n2] = struct{}{}
		case "Remove":
			l.Resp.CombSolution.RemoveBases[n2] = struct{}{}
		}
	}
	return nil
}

func (l *LibMinParser) ParseCombStackChange(s string, s1 string, s2 string) error {
	if s == VersionMinimize {
		var n1 = l.Sq.Add(TrimDq(s1))
		var n2 = l.Sq.Add(TrimDq(s2))
		l.Req.CombMinimize.StackChange[n1] = n2
		l.Req.CombMinimize.StackAmount[n1] = 0
		l.Req.CombMinimize.StackTo[n1] = 0
	}
	if s == VersionSolution {
		var n2 = l.Sq.Add(TrimDq(s2))
		switch s1 {
		case "Keep":
			l.Resp.CombSolution.KeepStacks[n2] = struct{}{}
		case "Remove":
			l.Resp.CombSolution.RemoveStacks[n2] = struct{}{}
		}
	}
	return nil
}

func (l *LibMinParser) ParseCombTarget(s string, s1 string) error {
	if s == VersionMinimize {
		var n1 = l.Sq.Add(TrimDq(s1))
		l.Req.CombMinimize.Target[n1] = struct{}{}
	}
	return nil
}
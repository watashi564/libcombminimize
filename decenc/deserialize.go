package decenc

import (
	"errors"
	"bitbucket.org/watashi564/libcombminimize/json"
	"fmt"
	"github.com/db47h/ragel/v2"
	"io"
	"strings"
)

var ErrMultipleJsonP = errors.New("mutlipleJsonP")
var ErrjJsonPfooterWithoutHeader = errors.New("jsonPfooterWithoutHeader")
var ErrJsonClosingBrace = errors.New("jsonClosingBrace")
var ErrJsonClosingBracket = errors.New("jsonClosingBracket")
var ErrGarbageJsonP = errors.New("garbageJsonP")
var ErrMissingJsonColon = errors.New("missingJsonColon")
var ErrMissingJsonStringKey = errors.New("missingJsonStringKey")
var ErrUnitializedJsonP = errors.New("unitializedJsonP")
var ErrjJsonPbadEnd = errors.New("jsonPbadEnd")
var ErrNeedColonBeforeClause = errors.New("needColonBeforeClause")
var ErrjJsonPdQuote = errors.New("jsonPdQuote")
var ErrjJsonPparen = errors.New("jsonPparen")
var ErrjJsonUnknown = errors.New("jsonUnknownToken")

func IsJsonError(e error) bool {
	switch e {
	case ErrMultipleJsonP:
		return true
	case ErrjJsonPfooterWithoutHeader:
		return true
	case ErrJsonClosingBrace:
		return true
	case ErrJsonClosingBracket:
		return true
	case ErrGarbageJsonP:
		return true
	case ErrMissingJsonColon:
		return true
	case ErrMissingJsonStringKey:
		return true
	case ErrUnitializedJsonP:
		return true
	case ErrjJsonPbadEnd:
		return true
	case ErrNeedColonBeforeClause:
		return true
	case ErrjJsonPdQuote:
		return true
	case ErrjJsonPparen:
		return true
	case ErrjJsonUnknown:
		return true
	}
	return false
}

type Parser interface {
	ParseInit(addressString string, jsonP string, jsonPbool bool) error
	ParseCombBase(string, string, string) error
	ParseCombStackTo(string, string, string) error
	ParseCombStackAmount(string, string, string) error
	ParseCombStackChange(string, string, string) error
	ParseCombTarget(string, string) error
}

func SolutionDeserializeV1(r io.Reader, p Parser) error {
	return MinimizeDeserializeV1(r, p)
}

func MinimizeDeserializeV1(r io.Reader, p Parser) error {
	var onlyOneJsonP bool
	var braceCounter, bracketCounter int
	var objectString string
	var addressString string
	var clauseNoEntriesSoFar bool
	var clauseStringsParity bool
	var alreadyJson bool
	var colon bool
	var initialized bool
	var lvl2address bool
	var jsonPstring string
	var jsonPbool bool
	var needColon bool
	var wasColon bool
	s := ragel.New("stdin", r, json.Json{})
	for {
		pos, tok, literal := s.Next()
		switch tok {
		case json.EntityJsonJsonpTrailer:
			if len(jsonPstring) == 0 {
				return ErrjJsonPfooterWithoutHeader

			} else {

				switch strings.TrimSpace(literal) {
				case ";":
					if jsonPbool {
						return ErrjJsonPbadEnd
					}
				default:
					if !jsonPbool {
						return ErrjJsonPbadEnd
					}
				}
			}
			fallthrough
		case ragel.EOF:
			if bracketCounter != 0 {
				return ErrJsonClosingBracket
			}
			if braceCounter != 0 {
				return ErrJsonClosingBrace
			}

			return nil
		case ragel.Error:
			return fmt.Errorf("scan error: %v: %v\n", s.Pos(pos), literal)
		case json.EntityJsonNumber:

			if braceCounter == 2 && bracketCounter == 1 {
				if !clauseStringsParity {
					err := p.ParseCombBase(objectString, addressString, literal)
					if err != nil {
						return err
					}
				} else {
					err := p.ParseCombStackAmount(objectString, addressString, literal)
					if err != nil {
						return err
					}
				}
				clauseNoEntriesSoFar = false
			}

		case json.EntityJsonString:
			alreadyJson = true
			if braceCounter == 1 {
				if !initialized {
					initialized = true
					err := p.ParseInit(literal, jsonPstring, jsonPbool)
					if err != nil {
						return err
					}
				}
				objectString = literal
			}
			if braceCounter == 2 && bracketCounter == 0 {
				lvl2address = true
				addressString = literal
			}
			if braceCounter == 2 && bracketCounter == 1 {
				if clauseStringsParity {
					err := p.ParseCombStackTo(objectString, addressString, literal)
					if err != nil {
						return err
					}
				} else {
					err := p.ParseCombStackChange(objectString, addressString, literal)
					if err != nil {
						return err
					}
				}
				clauseNoEntriesSoFar = false
				clauseStringsParity = !clauseStringsParity
			}

		case json.EntityJsonColon:
			if braceCounter == 2 && bracketCounter == 0 && !lvl2address {
				return ErrMissingJsonStringKey
			}
			if needColon {
				needColon = false
				wasColon = true
			}
			colon = true
		case json.EntityJsonComma:
			if braceCounter == 1 {
				objectString = ""
				break
			}
			if braceCounter == 2 && bracketCounter == 0 {
				addressString = ""
				break
			}

		case json.EntityJsonLeftBrace:
			alreadyJson = true
			braceCounter++
			if braceCounter == 2 {
				if bracketCounter == 0 {
					needColon = true
				}
			}

		case json.EntityJsonLeftBracket:
			if braceCounter == 2 {
				if bracketCounter == 0 {
					if !colon {
						return ErrMissingJsonColon
					}
					if !wasColon {
						return ErrNeedColonBeforeClause
					}
					wasColon = false

					if !lvl2address {
						return ErrMissingJsonStringKey
					}
					clauseNoEntriesSoFar = true
					clauseStringsParity = false
				}
				bracketCounter++
			}
		case json.EntityJsonRightBracket:
			if braceCounter == 2 {
				if bracketCounter == 0 {
					return ErrJsonClosingBracket
				}

				if bracketCounter == 1 {
					needColon = true

					if clauseNoEntriesSoFar {
						err := p.ParseCombTarget(objectString, addressString)
						if err != nil {
							return err
						}
						clauseNoEntriesSoFar = false
					}
				}

				bracketCounter--
			}

		case json.EntityJsonRightBrace:
			if braceCounter == 0 {
				return ErrJsonClosingBrace
			}
			braceCounter--
			if braceCounter == 0 {
				colon = false
			}
		case json.EntityJsonJsonpHeader:
			if onlyOneJsonP {
				return ErrMultipleJsonP
			}
			if alreadyJson {
				return ErrGarbageJsonP
			}
			if len(jsonPstring) > 0 {
				return ErrMultipleJsonP
			}
			if strings.Contains(literal, `"`) {
				return ErrjJsonPdQuote
			}
			if strings.Contains(literal, `(`) {
				return ErrjJsonPparen
			}
			onlyOneJsonP = true
			literal = strings.TrimSpace(literal)

			if len(literal) > 0 {
				var isFunc = literal[len(literal)-1] == '('
				var isVar = literal[len(literal)-1] == '='

				if isFunc {
					jsonPstring = strings.TrimSpace(strings.TrimRight(literal, "("))
					jsonPbool = len(jsonPstring) > 0
				}
				if isVar {
					jsonPstring = strings.TrimSpace(strings.TrimRight(literal, "="))
					jsonPbool = false
				}

			}
		case json.EntityJsonFalse, json.EntityJsonTrue, json.EntityJsonNull:
			// igonre
		default:
			return ErrjJsonUnknown
			//fmt.Printf("%d: %v: %s %s\n", pos, s.Pos(pos), json.JsonEntity[tok], literal)
		}
	}
}
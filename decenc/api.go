package decenc

import (
	"bytes"
	"encoding/hex"
	"io"
	"io/ioutil"
	"strconv"
)

type encoder struct {
	rwr       io.ReadWriter
	isError   bool
	noComma   bool
	jsonp     bool
	jsonpfunc bool
	err       error
}

func init() {
	// bypass linter unused isError false alarm
	var used encoder
	_ = used.isError
}


type TargetEncoder encoder
type StackEncoder encoder
type BaseEncoder encoder
type KeepEncoder encoder
type RemoveEncoder encoder

const lparen = "("
const assignment = "="
const rparencall = ");"
const semi = ";"
const prologuem = `{"CombMinimizeV1":{`
const prologues = `{"CombSolutionV1":{`
const epilogue = `}}`
const comma = `,`
const clausuleStart = `"`
const clausuleEnd = `"`
const clausulePayload = `": [`
const clausulePayloadChange = `": ["`
const clausuleTargetEnd = `": []`
const clausuleStackChangeAmt = `", `
const clausuleStackAmtTo = `, "`
const clausuleStackEnd = `"]`
const clausuleBaseEnd = `]`
const keepBegin = `"Keep":[`
const removeBegin = `"Remove":[`
const keepEnd = `],`
const removeEnd = `]`

func (e *encoder) handler(n int, err error) {
	if err != nil {
		e.err = err
	}
}

func (e *encoder) jsonP(jsonp string, jsonpFunc bool) {
	e.handler(e.rwr.Write([]byte(jsonp)))
	if len(jsonp) > 0 {
		e.jsonp = true
		if jsonpFunc {
			e.handler(e.rwr.Write([]byte(lparen)))
			e.jsonpfunc = true
		} else {
			e.handler(e.rwr.Write([]byte(assignment)))
		}
	}

	e.noComma = true

}

func NewRequestEncoder(jsonp string, jsonpFunc bool, features ...interface{}) (e TargetEncoder) {
	e.rwr = new(bytes.Buffer)
	(*encoder)(&e).jsonP(jsonp, jsonpFunc)
	(*encoder)(&e).handler(e.rwr.Write([]byte(prologuem)))
	return e
}
func (e *TargetEncoder) Add(target []byte) {
	if e.isError {
		panic("TargetEncoder.Add called after completion")
	}
	if e.noComma {
		e.noComma = false
	} else {
		(*encoder)(e).handler(e.rwr.Write([]byte(comma)))
	}
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleStart)))
	(*encoder)(e).handler(e.rwr.Write([]byte(hex.EncodeToString(target))))
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleTargetEnd)))
}
func (te *TargetEncoder) Complete() (se StackEncoder) {
	if te.isError {
		panic("TargetEncoder.Complete called twice")
	}
	se = StackEncoder(*te)
	te.isError = true
	return se
}

func (e *StackEncoder) Add(key, to, change []byte, amount uint64) {
	if e.isError {
		panic("StackEncoder.Add called after completion")
	}
	if e.noComma {
		e.noComma = false
	} else {
		(*encoder)(e).handler(e.rwr.Write([]byte(comma)))
	}

	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleStart)))
	(*encoder)(e).handler(e.rwr.Write([]byte(hex.EncodeToString(key))))
	(*encoder)(e).handler(e.rwr.Write([]byte(clausulePayloadChange)))
	(*encoder)(e).handler(e.rwr.Write([]byte(hex.EncodeToString(change))))
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleStackChangeAmt)))

	if amount == 0 {
		(*encoder)(e).handler(e.rwr.Write([]byte("0")))
	} else {
		if amount >= 10 {
			(*encoder)(e).handler(e.rwr.Write([]byte(strconv.FormatInt(int64(amount/10), 10))))
		}
		(*encoder)(e).handler(e.rwr.Write([]byte(strconv.FormatInt(int64(amount%10), 10))))
	}
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleStackAmtTo)))
	(*encoder)(e).handler(e.rwr.Write([]byte(hex.EncodeToString(to))))
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleStackEnd)))

}
func (se *StackEncoder) Complete() (be BaseEncoder) {
	if se.isError {
		panic("StackEncoder.Complete called twice")
	}
	be = BaseEncoder(*se)
	se.isError = true
	return be
}
func (e *BaseEncoder) Add(base []byte, amount uint64) {
	if e.isError {
		panic("BaseEncoder.Add called after completion")
	}
	if e.noComma {
		e.noComma = false
	} else {
		(*encoder)(e).handler(e.rwr.Write([]byte(comma)))
	}
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleStart)))
	(*encoder)(e).handler(e.rwr.Write([]byte(hex.EncodeToString(base))))
	(*encoder)(e).handler(e.rwr.Write([]byte(clausulePayload)))
	if amount == 0 {
		(*encoder)(e).handler(e.rwr.Write([]byte("0")))
	} else {
		if amount >= 10 {
			(*encoder)(e).handler(e.rwr.Write([]byte(strconv.FormatInt(int64(amount/10), 10))))
		}
		(*encoder)(e).handler(e.rwr.Write([]byte(strconv.FormatInt(int64(amount%10), 10))))
	}
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleBaseEnd)))
}
func (be *BaseEncoder) complete() {
	if be.isError {
		panic("BaseEncoder.Complete called twice")
	}
	be.isError = true
	(*encoder)(be).handler(be.rwr.Write([]byte(epilogue)))

	if be.jsonp {
		if be.jsonpfunc {
			(*encoder)(be).handler(be.rwr.Write([]byte(rparencall)))
		} else {
			(*encoder)(be).handler(be.rwr.Write([]byte(semi)))
		}
	}
}

func (be *BaseEncoder) Complete(resultNeeded bool) ([]byte, error) {
	be.complete()

	if resultNeeded {
		return ioutil.ReadAll(be.rwr)
	}
	return nil, be.err
}
func (be *TargetEncoder) Reader() io.Reader {
	return be.rwr
}

func (e *BaseEncoder) ContentType() string {
	if e.jsonp {
		return "application/javascript"
	}
	return "application/json"
}
func (e *TargetEncoder) ContentType() string {
	if e.jsonp {
		return "application/javascript"
	}
	return "application/json"
}

func NewResponseEncoder(jsonp string, jsonpFunc bool, features ...interface{}) (e KeepEncoder) {
	e.rwr = new(bytes.Buffer)
	(*encoder)(&e).jsonP(jsonp, jsonpFunc)
	(*encoder)(&e).handler(e.rwr.Write([]byte(prologues)))
	(*encoder)(&e).handler(e.rwr.Write([]byte(keepBegin)))
	e.noComma = true
	return e
}
func (ke *KeepEncoder) Reader() io.Reader {
	return ke.rwr
}

func (e *KeepEncoder) AddStack(target []byte) {
	if e.isError {
		panic("KeepEncoder.AddStack called after completion")
	}
	if e.noComma {
		e.noComma = false
	} else {
		(*encoder)(e).handler(e.rwr.Write([]byte(comma)))
	}
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleStart)))
	(*encoder)(e).handler(e.rwr.Write([]byte(hex.EncodeToString(target))))
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleEnd)))
}

func (e *KeepEncoder) AddBase(amount uint64) {
	if e.isError {
		panic("KeepEncoder.AddBase called after completion")
	}
	if e.noComma {
		e.noComma = false
	} else {
		(*encoder)(e).handler(e.rwr.Write([]byte(comma)))
	}
	if amount == 0 {
		(*encoder)(e).handler(e.rwr.Write([]byte("0")))
	} else {
		if amount >= 10 {
			(*encoder)(e).handler(e.rwr.Write([]byte(strconv.FormatInt(int64(amount/10), 10))))
		}
		(*encoder)(e).handler(e.rwr.Write([]byte(strconv.FormatInt(int64(amount%10), 10))))
	}
}

func (te *KeepEncoder) Complete() (se RemoveEncoder) {
	if te.isError {
		panic("KeepEncoder.Complete called twice")
	}
	(*encoder)(te).handler(te.rwr.Write([]byte(keepEnd)))
	(*encoder)(te).handler(te.rwr.Write([]byte(removeBegin)))
	se = RemoveEncoder(*te)
	te.isError = true
	se.noComma = true
	return se
}

func (e *RemoveEncoder) AddStack(target []byte) {
	if e.isError {
		panic("KeepEncoder.AddStack called after completion")
	}
	if e.noComma {
		e.noComma = false
	} else {
		(*encoder)(e).handler(e.rwr.Write([]byte(comma)))
	}
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleStart)))
	(*encoder)(e).handler(e.rwr.Write([]byte(hex.EncodeToString(target))))
	(*encoder)(e).handler(e.rwr.Write([]byte(clausuleEnd)))
}
func (e *RemoveEncoder) AddBase(amount uint64) {
	if e.isError {
		panic("RemoveEncoder.AddBase called after completion")
	}
	if e.noComma {
		e.noComma = false
	} else {
		(*encoder)(e).handler(e.rwr.Write([]byte(comma)))
	}
	if amount == 0 {
		(*encoder)(e).handler(e.rwr.Write([]byte("0")))
	} else {
		if amount >= 10 {
			(*encoder)(e).handler(e.rwr.Write([]byte(strconv.FormatInt(int64(amount/10), 10))))
		}
		(*encoder)(e).handler(e.rwr.Write([]byte(strconv.FormatInt(int64(amount%10), 10))))
	}
}

func (be *RemoveEncoder) complete() {
	if be.isError {
		panic("RemoveEncoder.Complete called twice")
	}
	be.isError = true
	(*encoder)(be).handler(be.rwr.Write([]byte(removeEnd)))
	(*encoder)(be).handler(be.rwr.Write([]byte(epilogue)))

	if be.jsonp {
		if be.jsonpfunc {
			(*encoder)(be).handler(be.rwr.Write([]byte(rparencall)))
		} else {
			(*encoder)(be).handler(be.rwr.Write([]byte(semi)))
		}
	}
}
func (re *RemoveEncoder) Complete(resultNeeded bool) ([]byte, error) {
	re.complete()

	if resultNeeded {
		return ioutil.ReadAll(re.rwr)
	}
	return nil, re.err
}
package callpython

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

var ErrPythonNotOptimal = fmt.Errorf("the problem does not have an optimal solution")
var ErrPythonErrored = fmt.Errorf("python errored with no stdout output")

func CallPython(script, python string) (map[string]float64, error) {

	var buf bytes.Buffer
	scanner := bufio.NewScanner(&buf)

	cmd := exec.Command(python)
	cmd.Stdin = strings.NewReader(script)
	cmd.Stdout = &buf
	cmd.Stderr = os.Stderr
	_ = cmd.Run()

	scanner.Split(bufio.ScanLines)
	var text []string

	for scanner.Scan() {
		text = append(text, scanner.Text())
	}

	if len(text) == 0 {
		return nil, ErrPythonErrored
	}

	var results = make(map[string]float64)
	for i := 0; i < len(text); i++ {
		split := strings.Split(text[i], "=")
		results[strings.TrimSpace(split[0])], _ = strconv.ParseFloat(strings.TrimSpace(split[1]), 64)
	}

	if results["IsSolution"] == 0 {
		return results, ErrPythonNotOptimal
	}

	return results, nil
}
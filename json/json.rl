// This is used to generate json.go
package json

import "github.com/db47h/ragel/v2"

const (
    EntityJsonString = ragel.Token(iota)
    EntityJsonNumber
    EntityJsonTrue
    EntityJsonFalse
    EntityJsonNull
    EntityJsonLeftBrace
    EntityJsonRightBrace
    EntityJsonLeftBracket
    EntityJsonRightBracket
    EntityJsonComma
    EntityJsonColon
    EntityJsonJsonpHeader
    EntityJsonJsonpTrailer
)

var JsonEntity = [...]string{
	"String",
	"Number",
	"True",
	"False",
	"Null",
	"LeftBrace",
	"RightBrace",
	"LeftBracket",
	"RightBracket",
	"Comma",
	"Colon",
	"JsonpHeader",
	"JsonpTrailer",
}

func jsonString(data []byte, ts, te, p int) string {
	var end = te
	if end < p {
		end = p
	}
	if end < ts {
		end = ts
	}
	return string(data[ts:end])
}

%%{
  machine json;

  newline = '\n' @{ s.Newline(p) };
  json_spaces = [\r\t ]+;

  hex_digit = '0' .. '9' | 'a' .. 'f' | 'A' .. 'F';
  decimal_digit = '0' .. '9';
  decimal_digit1to9 = '1' .. '9';
  decimal_digit0 = '0';

  opt_sign_minus = '-'?;

  decimal_opt_num = ( decimal_digit )*;
  decimal_some_num = ( decimal_digit )+;

  decimal_digits = decimal_digit0 | ( decimal_digit1to9  decimal_opt_num ) ;

  decimal_lit    = decimal_digits ;
  int_lit        = decimal_lit ;

  float_fraction = '.' decimal_some_num;


  decimal_exponent  = [eE] [ '+' | '-' ]? decimal_some_num ;

  oexp_lit = decimal_exponent?;
  ofrac_lit = float_fraction?;


  str_good_char = '\\''"' | '\\''\\' | '\\''/' | '\\''b' | '\\''f' | '\\''n' | '\\''r' | '\\''t'
  					| '\\''u' hex_digit hex_digit hex_digit hex_digit;
  str_bad_char = cntrl | '\\' | '"';
  str_ok_char = str_good_char | (ascii - str_bad_char);

  json_dq_str = '"' (str_ok_char*) '"';

  json_string = json_dq_str;

  json_string_entity = json_string;

  json_keyword_true_entity = 'true';
  json_keyword_false_entity = 'false';
  json_keyword_null_entity = 'null';

  json_number_entity = opt_sign_minus int_lit ofrac_lit oexp_lit;

  json_jsonp_header = (ascii - [{}\[\],:;()"])+ ('(' | '=');

  json_jsonp_trailer = ')' | ';' | ')'';';

  main := |*
        newline							%{};
		json_spaces						%{};
		json_string_entity				%{ s.Emit(ts, EntityJsonString, jsonString(data, ts, te, p))     };
		json_number_entity				%{ s.Emit(ts, EntityJsonNumber, jsonString(data, ts, te, p))     };
		json_keyword_true_entity		%{ s.Emit(ts, EntityJsonTrue, jsonString(data, ts, te, p))    };
		json_keyword_false_entity		%{ s.Emit(ts, EntityJsonFalse, jsonString(data, ts, te, p))    };
		json_keyword_null_entity		%{ s.Emit(ts, EntityJsonNull, jsonString(data, ts, te, p))    };

		'{'				%{ s.Emit(ts, EntityJsonLeftBrace, jsonString(data, ts, te, p))    };
		'}'				%{ s.Emit(ts, EntityJsonRightBrace, jsonString(data, ts, te, p))    };
		'['				%{ s.Emit(ts, EntityJsonLeftBracket, jsonString(data, ts, te, p))    };
		']'				%{ s.Emit(ts, EntityJsonRightBracket, jsonString(data, ts, te, p))    };
		','				%{ s.Emit(ts, EntityJsonComma, jsonString(data, ts, te, p))    };
		':'				%{ s.Emit(ts, EntityJsonColon, jsonString(data, ts, te, p))    };

		json_jsonp_header				%{ s.Emit(ts, EntityJsonJsonpHeader, jsonString(data, ts, te, p))    };
		json_jsonp_trailer				%{ s.Emit(ts, EntityJsonJsonpTrailer, jsonString(data, ts, te, p))    };
	*|;
}%%

%%write data;

// Json implements ragel.Interface. This is the canonical implementation
// available at https://godoc.org/github.com/db47h/ragel#Interface
// with the type name changed to Json.
//
type Json struct {}

func (Json) Init(s *ragel.State) (int, int) {
    var cs, ts, te, act int
    %%write init;
    s.SaveVars(cs, ts, te, act)
    return %%{ write start; }%%, %%{ write error; }%%
}

func (Json) Run(s *ragel.State, p, pe, eof int) (int, int) {
    cs, ts, te, act, data := s.GetVars()
    %%write exec;
    s.SaveVars(cs, ts, te, act)
    return p, pe
}
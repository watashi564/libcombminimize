package flow

import (
	"bitbucket.org/watashi564/libcombminimize/decenc"
	"strings"
)

// Process is the main flow processor, generates a python script
func Process(b, sTo, sAmt, sChg map[uint64]uint64, t map[uint64]struct{}) (string, error) {
	var bases = b
	var stacksTo = sTo
	var stacksAmt = sAmt
	var stacksChg = sChg
	var targets = t

	var tx, err = GetTx(stacksAmt, stacksChg)
	if err != nil {
		return "", err
	}
	stacksAmt = ClearZeroStacksByTx(tx, stacksAmt)
	stacksChg = ClearZeroStacksByTx(tx, stacksChg)
	stacksTo = ClearZeroStacksByTx(tx, stacksTo)
	tx = SinkTx(tx)
	stacksChg = SinkStacks(tx, stacksChg)
	stacksTo = SinkStacks(tx, stacksTo)
	var sunkBases = SinkBases(tx, bases)
	var summedBases = SumSinkBases(tx, bases)
	var eq = BasesToEquations(sunkBases)
	var maps = NewMaps(summedBases, stacksAmt, bases, targets, sunkBases)
	var triggeredStacks = make(map[uint64]struct{})
	eq.KeepForwardFlowBalances(maps.Balance, stacksTo, stacksChg, stacksAmt, triggeredStacks)
	eq.FlowDeps()
	for eq.FlowBitands() {
	}
	var script = eq.PythonCommand(&maps)
	return script, nil
}

func Postprocess(bits map[string]float64, bases, sAmt map[uint64]uint64) (ks, kb, rs, rb map[uint64]struct{}, err error) {
	var n uint64
	ks, kb, rs, rb = make(map[uint64]struct{}), make(map[uint64]struct{}), make(map[uint64]struct{}), make(map[uint64]struct{})
	for k, v := range bits {
		if strings.HasPrefix(k, "claim") {

			n, err = decenc.Uint64decode(k[5:])
			if err != nil {
				return
			}
			if v > 0.5 {
				kb[n] = struct{}{}
			} else {
				rb[n] = struct{}{}
			}
			//println(k, v, n)
		} else if strings.HasPrefix(k, "stack") {

			n, err = decenc.Uint64decode(k[5:])
			if err != nil {
				return
			}
			if v > 0.5 {
				ks[n] = struct{}{}
			} else {
				rs[n] = struct{}{}
			}
			//println(k, v, n)
		}
	}

	return ks, kb, rs, rb, nil
}
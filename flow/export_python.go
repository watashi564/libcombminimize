package flow

const Header = `
from ortools.linear_solver import pywraplp
solver = pywraplp.Solver.CreateSolver('SCIP')
infinity = solver.infinity()
`
const Middle = `status = solver.Solve()
print('Number of constraints =', solver.NumConstraints())
if status == pywraplp.Solver.OPTIMAL:
    print('IsSolution = 1.0')
    print('ObjectiveValue =', solver.Objective().Value())
`

const Footer = `else:
    print('IsSolution = 0.0')
`

func (eq *Equations) PythonCommand(maps *Maps) string {
	return maps.ExportPythonVars() +
		eq.ExportPythonVars(maps) +
		maps.ExportPython() +
		eq.ExportPython(maps) +
		maps.ExportPythonMinimize()
}
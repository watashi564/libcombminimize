package flow

type FuzzFlowIBalance map[uint64]int64
type FuzzFlowEquation map[uint64]FuzzFlowIBalance
type FuzzFlowAddrCond map[uint64]map[uint64]struct{}
type FuzzFlowDeps map[uint64]uint64
type FuzzFlowBitand map[uint64][2]uint64
type FuzzMerkle map[[2]uint64]uint64

type Equations struct {
	EqBitandBitandStack FuzzFlowEquation
	EqClaims            FuzzFlowEquation
	EqStacks            FuzzFlowEquation
	EqAddrCond          FuzzFlowAddrCond
	AddressDeps         FuzzFlowDeps
	BitandClaimStack    FuzzFlowBitand
	BitandStackStack    FuzzFlowBitand
	BitandBitandStack   FuzzFlowBitand
	Merkle1             FuzzMerkle
	Merkle2             FuzzMerkle
	Merkle3             FuzzMerkle
	Merkle4             FuzzMerkle
}

func (ff *Equations) AddToEquation(isClaim bool, isStack bool, addr, cond uint64, money int64) {
	var eq *FuzzFlowEquation
	if isClaim {
		eq = &ff.EqClaims
	} else if isStack {
		eq = &ff.EqStacks
	} else {
		eq = &ff.EqBitandBitandStack
	}
	if eqmap, ok := (*eq)[addr]; ok {
		eqmap[cond] += money
		return
	}
	eqmap := make(FuzzFlowIBalance)
	eqmap[cond] += money
	(*eq)[addr] = eqmap
}
func (ff *Equations) AddAddrStackToEquation(myaddr, stackaddr uint64) {
	if eqmap, ok := ff.EqAddrCond[myaddr]; ok {
		eqmap[stackaddr] = struct{}{}
		return
	}
	eqmap := make(map[uint64]struct{})
	eqmap[stackaddr] = struct{}{}
	ff.EqAddrCond[myaddr] = eqmap
}

func (ff *Equations) FlowDeps() {
	for k, v := range ff.EqAddrCond {
		for w := range v {
			//if _, ok := ff.Targets[k]; !ok {

			ff.AddressDeps[k] = w

			//}
		}
	}
}

func (ff *Equations) ExportPythonVars(maps *Maps) (o string) {
	for k := range maps.Balance {
		if _, ok := maps.StackAmount[k]; ok {
			continue
		}
		var xk = toStrUInt(k)
		o += `addr` + xk + ` = solver.IntVar(0, infinity, 'addr` + xk + `')`
		o += "\n"
	}

	for k := range ff.BitandStackStack {
		var xk = toStrUInt(k)
		o += `bitand` + xk + ` = solver.IntVar(0.0, 1.0, 'bitand` + xk + `')`
		o += "\n"
	}
	for k := range ff.BitandClaimStack {
		var xk = toStrUInt(k)
		o += `bitand` + xk + ` = solver.IntVar(0.0, 1.0, 'bitand` + xk + `')`
		o += "\n"
	}
	for k := range ff.BitandBitandStack {
		var xk = toStrUInt(k)
		o += `bitand` + xk + ` = solver.IntVar(0.0, 1.0, 'bitand` + xk + `')`
		o += "\n"
	}
	return o
}

func (ff *Equations) ExportPython(maps *Maps) (o string) {
	for k, v := range maps.Balance {
		var xk = toStrUInt(k)

		{
			o += `solver.Add(0 == `

			o += ff.ExportPythonEquation(k)

			o += `-1 * addr` + xk + `)`
			o += "\n"
		}

		if _, ok := maps.Target[k]; ok {
			var xv = toStrUInt(v)
			o += `solver.Add(` + xv + ` == addr` + xk + `)`
			o += "\n"
		}

	}

	for k, v := range ff.BitandStackStack {
		if _, ok := maps.StackAmount[v[0]]; !ok {
			continue
		}
		var xk = toStrUInt(k)
		var xv0 = toStrUInt(v[0])
		var xv1 = toStrUInt(v[1])
		o += `solver.Add(0 >= 2*bitand` + xk + ` - stack` + xv0 + ` - stack` + xv1 + `)`
		o += "\n"
	}
	for k, v := range ff.BitandClaimStack {
		/*
			if _, ok := maps.SunkBases[v[0]]; !ok {
				continue
			}*/
		var xk = toStrUInt(k)
		var xv0 = toStrUInt(v[0])
		var xv1 = toStrUInt(v[1])
		o += `solver.Add(0 >= 2*bitand` + xk + ` - claim` + xv0 + ` - stack` + xv1 + `)`
		o += "\n"
	}
	for k, v := range ff.BitandBitandStack {
		var xk = toStrUInt(k)
		var xv0 = toStrUInt(v[0])
		var xv1 = toStrUInt(v[1])

		o += `solver.Add(0 >= 2*bitand` + xk + ` - bitand` + xv0 + ` - stack` + xv1 + `)`
		o += "\n"
	}
	return o
}

func (ff *Equations) ExportPythonEquation(k uint64) (o string) {
	if d, ok := ff.EqClaims[k]; ok {
		for k, v := range d {
			var xk = toStrUInt(k)
			var xv = toStrInt(v)

			o += xv + " * claim" + xk + " + "
		}
	}
	if d, ok := ff.EqStacks[k]; ok {
		for k, v := range d {
			var xk = toStrUInt(k)
			var xv = toStrInt(v)

			o += xv + " * stack" + xk + " + "
		}

	}
	if d, ok := ff.EqBitandBitandStack[k]; ok {
		for k, v := range d {
			var xk = toStrUInt(k)
			var xv = toStrInt(v)

			o += xv + " * bitand" + xk + " + "
		}

	}
	return o
}
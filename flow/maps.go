package flow

import (
	"fmt"
)

type Maps struct {
	Base        map[uint64]uint64
	StackAmount map[uint64]uint64
	Balance     map[uint64]uint64
	Target      map[uint64]struct{}
	SunkBases   map[uint64]map[uint64]struct{}
}

func NewMaps(summedBases, stackAmount, base map[uint64]uint64, target map[uint64]struct{},
	sunkBases map[uint64]map[uint64]struct{}) (m Maps) {
	m.Balance = summedBases
	m.Target = target
	m.StackAmount = stackAmount
	m.Base = base
	m.SunkBases = sunkBases
	return m
}

func toStrInt(n int64) string {
	return fmt.Sprintf("%d", n)
}
func toStrUInt(n uint64) string {
	return fmt.Sprintf("%d", n)
}
func (m *Maps) ExportPythonVars() string {
	return Header +
		m.ExportPythonClaimVars() +
		m.ExportPythonStackVars() +
		m.ExportPythonStackInflowConstraintsVars()
}

func (m *Maps) ExportPython() string {
	return m.ExportPythonStackInflowConstraints()
}

func (m *Maps) ExportPythonMinimize() string {
	return `solver.Minimize(` +
		m.ExportPythonClaimMinimize() + m.ExportPythonStackMinimize() +
		`0)` + "\n" + Middle + m.ExportPythonClaimResult() + m.ExportPythonStackResult() + Footer

}

func (m *Maps) ExportPythonClaimVars() (o string) {
	for _, k := range m.SunkBases {
		for v := range k {
			var x = toStrUInt(v)
			o += `claim` + x + ` = solver.IntVar(0.0, 1.0, 'claim` + x + `')`
			o += "\n"
		}
	}
	return o
}

func (m *Maps) ExportPythonStackVars() (o string) {
	for k := range m.StackAmount {
		var x = toStrUInt(k)
		o += `stack` + x + ` = solver.IntVar(0.0, 1.0, 'stack` + x + `')`
		o += "\n"
	}
	return o
}

func (m *Maps) ExportPythonStackInflowConstraintsVars() (o string) {
	for k := range m.StackAmount {
		var x = toStrUInt(k)
		o += `addr` + x + ` = solver.IntVar(0, infinity, 'addr` + x + `')`
		o += "\n"
	}
	return o

}

func (m *Maps) ExportPythonStackInflowConstraints() (o string) {
	for k, v := range m.StackAmount {
		var x = toStrUInt(k)
		var a = toStrUInt(v)
		o += `solver.Add(addr` + x + ` >= ` + a + ` * stack` + x + `)`
		o += "\n"
	}
	return o
}

func (m *Maps) ExportPythonClaimMinimize() (o string) {
	for _, k := range m.SunkBases {
		for v := range k {
			var x = toStrUInt(v)
			o += `claim` + x + ` + `
		}
	}
	return o
}

func (m *Maps) ExportPythonStackMinimize() (o string) {
	for k := range m.StackAmount {
		var x = toStrUInt(k)
		o += `stack` + x + ` + `
	}
	return o
}

func (m *Maps) ExportPythonClaimResult() (o string) {
	for _, k := range m.SunkBases {
		for v := range k {
			var x = toStrUInt(v)
			o += `    print('claim` + x + ` =', claim` + x + `.solution_value())`
			o += "\n"
		}
	}
	return o

}

func (m *Maps) ExportPythonStackResult() (o string) {
	for k := range m.StackAmount {
		var x = toStrUInt(k)
		o += `    print('stack` + x + ` =', stack` + x + `.solution_value())`
		o += "\n"
	}
	return o

}
package flow

import "errors"

var ErrNoChange = errors.New("gettx: no change")

func GetTx(StackAmt, StackChange map[uint64]uint64) (tx map[uint64]uint64, err error) {
	tx = make(map[uint64]uint64)
	for k, amt := range StackAmt {
		if amt == 0 {
			addr, ok := StackChange[k]
			if !ok {
				err = ErrNoChange
			}
			tx[k] = addr
		}
	}
	return tx, err
}
func ClearZeroStacksByTx(Tx, Stack map[uint64]uint64) (stack map[uint64]uint64) {
	stack = Stack
	for k := range Tx {
		delete(stack, k)
	}
	return stack
}

func SinkTx(Tx map[uint64]uint64) (tx map[uint64]uint64) {
	tx = make(map[uint64]uint64)
	for source, target := range Tx {
		newTarget, ok := Tx[target]
		for ok && target != newTarget {
			target = newTarget
			newTarget, ok = Tx[target]
		}
		tx[source] = target
	}
	return tx
}
func SinkStacks(Tx, Stacks map[uint64]uint64) (stacks map[uint64]uint64) {
	stacks = make(map[uint64]uint64)
	for source, target := range Stacks {
		var newTarget, ok = Tx[target]
		if ok {
			stacks[source] = newTarget
		} else {
			stacks[source] = target
		}
	}
	return stacks
}

func SinkBases(Tx, Bases map[uint64]uint64) (sunkBases map[uint64]map[uint64]struct{}) {
	sunkBases = make(map[uint64]map[uint64]struct{})
	for amount, source := range Bases {
		if amount == 0 {
			continue
		}
		var newSource, ok = Tx[source]
		if ok {
			if _, ok2 := sunkBases[newSource]; !ok2 {
				sunkBases[newSource] = make(map[uint64]struct{})
			}
			sunkBases[newSource][amount] = struct{}{}
		} else {
			if _, ok2 := sunkBases[source]; !ok2 {
				sunkBases[source] = make(map[uint64]struct{})
			}
			sunkBases[source][amount] = struct{}{}
		}
	}
	return sunkBases
}

func SumSinkBases(Tx, Bases map[uint64]uint64) (summedBases map[uint64]uint64) {
	summedBases = make(map[uint64]uint64)
	for amount, source := range Bases {
		if amount == 0 {
			continue
		}
		var newSource, ok = Tx[source]
		if ok {
			summedBases[newSource] += amount
		} else {
			summedBases[source] += amount
		}
	}
	return summedBases
}

func (eq *Equations) ForwardFlowBalances(Balances, StackTo, StackChange, StackAmount map[uint64]uint64,
	TriggeredStacks map[uint64]struct{}) (trickled bool) {
	for addr, amount := range Balances {
		if amount == 0 {
			continue
		}
		var visited = make(map[uint64]struct{})
		for {
			if _, v := visited[addr]; v {
				Balances[addr] = 0
				trickled = true
				break
			}
			stackAmt, ok := StackAmount[addr]
			if !ok {
				break
			}
			_, isTriggered := TriggeredStacks[addr]
			if isTriggered {
				to, ok := StackChange[addr]
				if ok {
					Balances[to] += Balances[addr]
					Balances[addr] = 0
					visited[addr] = struct{}{}

					addr = to
					amount = Balances[to]
					trickled = true
					continue
				}
			} else if amount >= stackAmt {
				to, ok := StackTo[addr]
				if ok {
					TriggeredStacks[addr] = struct{}{}

					Balances[to] += stackAmt
					Balances[addr] -= stackAmt

					visited = make(map[uint64]struct{})

					var k = addr

					addr = to
					amount = Balances[to]
					trickled = true

					chg, ok2 := StackChange[k]
					if ok2 {
						eq.AddAddrStackToEquation(chg, k)
						eq.AddToEquation(false, true, chg, k, -int64overflow(stackAmt))

					}
					eq.AddToEquation(false, true, to, k, int64overflow(stackAmt))
					continue
				}

			}
			break
		}
	}
	return trickled
}
func (eq *Equations) KeepForwardFlowBalances(Balances, StackTo, StackChange, StackAmount map[uint64]uint64,
	TriggeredStacks map[uint64]struct{}) {

	for eq.ForwardFlowBalances(Balances, StackTo, StackChange, StackAmount, TriggeredStacks) {
	}
}

func int64overflow(n uint64) int64 {
	if n >= (1 << 63) {
		panic("overflow")
	}
	return int64(n)
}

func BasesToEquations(SunkBases map[uint64]map[uint64]struct{}) (eq Equations) {
	eq.EqStacks = make(FuzzFlowEquation)
	eq.EqAddrCond = make(FuzzFlowAddrCond)
	eq.EqClaims = make(FuzzFlowEquation)
	eq.EqBitandBitandStack = make(FuzzFlowEquation)
	eq.AddressDeps = make(FuzzFlowDeps)
	eq.BitandBitandStack = make(FuzzFlowBitand)
	eq.BitandClaimStack = make(FuzzFlowBitand)
	eq.BitandStackStack = make(FuzzFlowBitand)
	eq.Merkle1 = make(FuzzMerkle)
	eq.Merkle2 = make(FuzzMerkle)
	eq.Merkle3 = make(FuzzMerkle)
	eq.Merkle4 = make(FuzzMerkle)

	for k, v := range SunkBases {
		for k2 := range v {
			eq.AddToEquation(true, false, k, k2, int64overflow(k2))
		}
	}
	return eq
}

// TODO: Check if there can be only one shared merkle map
// I know that group 3 can be merged with the others
func (ff *Equations) Merkle(t, k uint64, g byte) uint64 {
	var val uint64
	switch g {
	case 1:
		val = ff.Merkle1[[2]uint64{t, k}]
	case 2:
		val = ff.Merkle2[[2]uint64{t, k}]
	case 3:
		val = ff.Merkle3[[2]uint64{t, k}]
	case 4:
		val = ff.Merkle4[[2]uint64{t, k}]
	default:
		panic("unknown merkle group")
	}
	if val != 0 {
		return val
	}
	var size = uint64(len(ff.Merkle1) + len(ff.Merkle2) + len(ff.Merkle3))
	val = (^uint64(0)) - size
	switch g {
	case 1:
		ff.Merkle1[[2]uint64{t, k}] = val
	case 2:
		ff.Merkle2[[2]uint64{t, k}] = val
	case 3:
		ff.Merkle3[[2]uint64{t, k}] = val
	case 4:
		ff.Merkle4[[2]uint64{t, k}] = val
	default:
		panic("unknown merkle group")
	}
	return val
}

func (ff *Equations) FlowBitands() bool {
	for v, k := range ff.AddressDeps {
		if _, ok := ff.AddressDeps[k]; ok {
			continue
		}

		// k is final key

		_ = v
		// locate equation k
		var eqClaims = ff.EqClaims[k]
	
		for t, w := range eqClaims {
			var m = ff.Merkle(t, k, 1)
			ff.BitandClaimStack[m] = [2]uint64{t, k}
			ff.AddToEquation(false, false, v, m, w)
		}
	
		var eqStacks = ff.EqStacks[k]
	
		for t, w := range eqStacks {
			var m = ff.Merkle(t, k, 2)
			ff.BitandStackStack[m] = [2]uint64{t, k}
			ff.AddToEquation(false, false, v, m, w)

		}

	

		var eqBitandBitandStack = ff.EqBitandBitandStack[k]
	
		for t, w := range eqBitandBitandStack {
			var m = ff.Merkle(t, k, 3)
			ff.BitandBitandStack[m] = [2]uint64{t, k}
			ff.AddToEquation(false, false, v, m, w)
		}
	

		ff.RmAddrStackFromEquation(v, k)
		delete(ff.AddressDeps, v)
		return true
	}
	return false
}
func (ff *Equations) RmAddrStackFromEquation(myaddr, stackaddr uint64) {
	if eqmap, ok := ff.EqAddrCond[myaddr]; ok {
		delete(eqmap, stackaddr)
		return
	}
}

func MaxMoney(Balance map[uint64]uint64) uint64 {
	var maxMoney uint64
	var maxKey uint64
	for k, v := range Balance {
		if v > maxMoney {
			maxMoney = v
			maxKey = k
		}
	}
	return maxKey
}
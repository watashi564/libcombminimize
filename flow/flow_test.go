package flow

import "testing"

func TestFlow(t *testing.T) {
	var bases = make(map[uint64]uint64)
	var stacksTo = make(map[uint64]uint64)
	var stacksAmt = make(map[uint64]uint64)
	var stacksChg = make(map[uint64]uint64)
	var targets = make(map[uint64]struct{})
	bases[13] = 1
	bases[14] = 1
	bases[15] = 1
	bases[16] = 1
	bases[17] = 2
	bases[18] = 2
	stacksChg[1] = 3
	stacksTo[1] = 2
	stacksAmt[1] = 30
	stacksChg[2] = 4
	stacksTo[2] = 3
	stacksAmt[2] = 10
	targets[3] = struct{}{}
	// let's go
	var script, err = Process(bases, stacksTo, stacksAmt, stacksChg, targets)
	if err != nil {
		t.Error(err)
	}
	println(script)
}